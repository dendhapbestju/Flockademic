import {
  GetOrcidProfileSearchResponse,
  GetOrcidResponse,
  GetOrcidWorkResponse,
} from '../../../../lib/interfaces/endpoints/orcid';
import { Person } from '../../../../lib/interfaces/Person';
import { ScholarlyArticle } from '../../../../lib/interfaces/ScholarlyArticle';
import { fetchResource } from './fetchResource';
import { getOpenAccessLocation } from './unpaywall';

// Returns an ORCID profile and the associated articles
export async function getProfile(orcid: string): Promise<[ Partial<Person>, Array<Partial<ScholarlyArticle>> ]> {
  const response = await fetchResource(`/accounts/orcid/${encodeURIComponent(orcid)}`);

  const orcidProfile: GetOrcidResponse = await response.json();

  // See https://github.com/Microsoft/TypeScript/issues/22111 for why the `!` is present here
  const lastName = (orcidProfile.person.name['family-name'])
    ? ' ' + orcidProfile.person.name['family-name']!.value
    : '';
  const profile: Partial<Person> = {
    // We're assuming the names to be ordered Western-style here, which doesn't always hold.
    // Unfortunately, I'm not quite sure how to deduce the appropriate order at this time.
    name: orcidProfile.person.name['given-names'].value + lastName,
  };

  if (orcidProfile.person.biography && orcidProfile.person.biography.content) {
    profile.description = orcidProfile.person.biography.content;
  }

  profile.sameAs = [ `https://orcid.org/${orcid}` ];
  if (orcidProfile.person['researcher-urls']['researcher-url'].length > 0) {
    profile.sameAs = profile.sameAs.concat(
      orcidProfile.person['researcher-urls']['researcher-url']
      .map((url) => ({
        roleName: url['url-name'],
        sameAs: url.url.value,
      }),
    ));
  }

  if (orcidProfile['activities-summary'].educations['education-summary'].length > 0) {
    profile.affliation = orcidProfile['activities-summary'].educations['education-summary'].map((education) => {
      return {
        affliation: {
          name: education.organization.name,
        },
        endDate: parseOrcidDate(education['end-date']),
        roleName: education['role-title'],
        startDate: parseOrcidDate(education['start-date']),
      };
    });
  }

  if (orcidProfile['activities-summary'].employments['employment-summary'].length > 0) {
    profile.affliation = profile.affliation || [];
    profile.affliation = profile.affliation.concat(
      orcidProfile['activities-summary'].employments['employment-summary'].map((employment) => {
        return {
          affliation: {
            name: employment.organization.name,
          },
          endDate: parseOrcidDate(employment['end-date']),
          roleName: employment['role-title'],
          startDate: parseOrcidDate(employment['start-date']),
        };
      }),
    );
  }

  const articles = orcidProfile['activities-summary'].works.group.map((workSummary) => {
    const work = workSummary['work-summary'][0];

    const article: Partial<ScholarlyArticle> = {
      name: work.title.title.value,
      sameAs: `https://orcid.org/${orcid}/work/${work['put-code']}`,
    };

    return article;
  });

  return [ profile, articles ];
}

export async function getWork(orcid: string, putCode: string): Promise<Partial<ScholarlyArticle>> {
  const response = await fetchResource(`/accounts/orcid/${encodeURIComponent(orcid)}/work/${putCode}`);

  const orcidWork: GetOrcidWorkResponse = await response.json();

  const article: Partial<ScholarlyArticle> = {
    author: [ { sameAs: `https://orcid.org/${orcid}` } ],
    name: orcidWork.title.title.value,
  };

  // See https://github.com/Microsoft/TypeScript/issues/22111 for why the `!` is present here
  if (orcidWork['short-description']) {
    article.description = orcidWork['short-description']!;
  }

  if (orcidWork['publication-date']) {
    article.datePublished = parseOrcidDate(orcidWork['publication-date']);
  }

  if (orcidWork['external-ids']) {
    const dois = orcidWork['external-ids']!['external-id']
      .filter((id) => id['external-id-type'] === 'doi' && id['external-id-value']);

    if (dois.length > 0) {
      const doi = dois[0]['external-id-value'];
      article.sameAs = `http://doai.io/${doi}`;
      try {
          const location = await getOpenAccessLocation(doi);
          if (location !== null) {
            article.associatedMedia = [ {
              contentUrl: location,
              license: undefined,
              name: doi + '.pdf',
            } ];
          }
      } catch (e) {
        // Do nothing - if there's a problem fetching the full article,
        // the user can always still try to find it through DOAI.
      }
    } else if (
      orcidWork['external-ids']!['external-id'].length > 0
      && orcidWork['external-ids']!['external-id'][0]['external-id-url']
    ) {
      // See https://github.com/Microsoft/TypeScript/issues/22111 for why the `!`s are present here
      article.sameAs = orcidWork['external-ids']!['external-id'][0]['external-id-url']!.value;
    }

    if (!article.associatedMedia) {
      const arxivIds = orcidWork['external-ids']!['external-id']
        .filter((idField) => idField['external-id-type'] === 'arxiv');
      if (arxivIds.length > 0) {
        article.associatedMedia = arxivIds.map((idField) => ({
          contentUrl: `https://arxiv.org/pdf/${idField['external-id-value']}`,
          // tslint:disable-next-line:max-line-length
          license: 'https://arxiv.org/licenses/nonexclusive-distrib/1.0/license.html' as 'https://arxiv.org/licenses/nonexclusive-distrib/1.0/license.html',
          name: `arxiv-${idField['external-id-value']}.pdf`,
        }));
      }
    }
  }

  if (!article.sameAs && orcidWork.url) {
    article.sameAs = orcidWork.url.value;
  }

  return article;
}

export async function searchProfiles(query: string): Promise<Array<Partial<Person>>> {
  const response = await fetchResource(`/accounts/orcid/search/${encodeURIComponent(query)}`);

  const profiles: GetOrcidProfileSearchResponse = await response.json();

  return profiles.map((profile) => {
    // See https://github.com/Microsoft/TypeScript/issues/22111 for why the `!` is present here
    const lastName = (profile.person.name['family-name'])
      ? ' ' + profile.person.name['family-name']!.value
      : '';

    return {
      name: profile.person.name['given-names'].value + lastName,
      sameAs: `https://orcid.org/${profile.person.name.path}`,
    };
  });
}

function parseOrcidDate(
  date: null | {
    year: null | { value: string; },
    month: null | { value: string; },
    day: null | { value: string; },
   },
) {
  if (!date || !date.year || !date.month || !date.day) {
    return undefined;
  }

  // Although ORCID doesn't specify the timezone for these properties
  // (https://members.orcid.org/api/tutorial/reading-xml#dates),
  // they do mention `UCT` (which presumably should be `UTC`) for other dates
  // (https://members.orcid.org/api/tutorial/reading-xml#last-modified),
  // so let's assume that here as well:
  const parsedDate = new Date(Date.UTC(
    parseInt(date.year.value, 10),
    parseInt(date.month.value, 10) - 1,
    parseInt(date.day.value, 10),
  ));

  return parsedDate.toISOString();
}
